module ApplicationHelper
   
   def current_user
    if(session[:user_id].nil?) 
        return false 
    end
    user = User.find(session[:user_id])
    if(user == nil)
      return false
    else
      return user
    end
   end
  
end
