class User < ActiveRecord::Base
    has_secure_password
    
    belongs_to :info
    
    validates :name, presence: true
    
    def editor?
      self.role == "admin"
    end
end