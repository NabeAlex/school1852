class PostsController < ApplicationController
      
    def index
      @posts = Post.all
    end
    
    def new
      @user = current_user
      if !(@user && @user.editor?)
        redirect_to ''
      end
      @post = Post.new
    end
    
    def create
      pass = post_params
      pass[:text].gsub!(/\n/, '<br />')
      pass[:text] = "" + pass[:text]
      if(@post = Post.create(pass))
        redirect_to '/posts'
      elsif
        redirect_to '/posts/new'
      end
    end
    
    private
      def post_params
        params.require(:post).permit(:topic, :text, :image)
      end
end
