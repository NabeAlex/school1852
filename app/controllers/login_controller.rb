class LoginController < ApplicationController
  
  def index
    p = login_user_params
    pass = User.find_by(email: p[:user_login])
    
    if pass && pass.password_digest == p[:user_password]
        session[:user_id] = pass.id
        redirect_to '/'
    else
        redirect_to '/signup'
    end
  end
  
  def delete
    session[:user_id] = nil
    redirect_to '/'
  end
  
  private
    def login_user_params
      params.require(:user).permit(:user_login, :user_password)
    end
    
    def edit_phone(phone)
      result = ""
      ar_num = Array("0".."9")
      phone.chars.each do |i|
    	if ar_num.include? i
    		result += i
    	end
      end
      result   
   end
end
