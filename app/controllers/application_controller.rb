class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  
  before_action :init
  
  include ApplicationHelper
  
  def init
    @current_path = controller_name + "#" + action_name
  end
  
end
