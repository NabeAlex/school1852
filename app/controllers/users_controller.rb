class UsersController < ApplicationController
    def new
      @user = User.new        
    end
    
    def create
      p = user_params
      p[:name] = p[:email]
      @user = User.create(p)
      @user.info = Info.create(user_info_params)
      if @user.save
        session[:user_id] = @user.id
        session[:error] = nil
        redirect_to '/'
      else
        session[:error] =  @user.errors.full_messages[0]
        redirect_to '/signup'
      end
    end
    
    def show
      id = params[:id]
      if(id == "all") 
        @user = User.all
        return
      end
      @user = User.find(id)
    end
    
    private
        def user_params
          params.require(:user).permit(:email, :password_digest, :password_confirmation)
        end
        def user_info_params
          params.require(:user).require(:info).permit(
            :phone, :first_name, :last_name, :school, :group, :class_number)
        end
end
