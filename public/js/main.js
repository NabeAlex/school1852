$(document).ready(function() {
    $(".open-topic").each(function() {
        $(this).click(function() {
            var vectorHref = $(this).attr("to");
            if(vectorHref[0] != "#")
                return;
            $(vectorHref).show(300 , function() {
                
            });
        });
    });
    
    $(".close").each(function() {
        $(this).click(function() {
            var vectorHref = $(this).attr("close");
            if(vectorHref[0] != "#")
                return;
            $(vectorHref).hide(300 , function() {
                
            });
        });
    });
    
    $(document).delegate('#post', 'keydown', function(e) {
      var keyCode = e.keyCode || e.which;
    
      if (keyCode == 9) {
        e.preventDefault();
        var start = $(this).get(0).selectionStart;
        var end = $(this).get(0).selectionEnd;
    
        $(this).val($(this).val().substring(0, start)
                    + "\this"
                    + $(this).val().substring(end));
    
        $(this).get(0).selectionStart =
        $(this).get(0).selectionEnd = start + 1;
      }
    });

});