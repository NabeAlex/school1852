var w = document.body.clientWidth;
var n = 0;


var ids = {};
var sliding = [];

$(".section").each(function() {
    var obj = $(this);
    var id = $(this).attr("id");
    obj.addClass("section-" + (obj.index() + 1));
    if(id !== undefined) {
        sliding.push(obj.index());
        ids[id] = { me : obj , position : (obj.index() + 1) };
    }
    
    n++;
});

$(".slider").css({width : (w) + "px"});
$(".info").css({width : (w*n) + "px"});
$(".section").css({width : (w) + "px"});

var main = $(".slider");
var slides = 0
$(".section").each(function() {
    var obj = $(this);
    slides++;
    //if(obj.index() == 0) main = obj;
    var id = $(this).attr("id");
    obj.addClass("section-" + (obj.index() + 1));
    if(id !== undefined) {
        ids[id] = { me : obj , position : (obj.index() + 1) };
    }
});

$(".slider-link").each(function() {
    $(this).click(function() {
        var vectorHref = $(this).attr("to");
        if(vectorHref[0] != "#")
            return;
        var idObj = vectorHref.substr(1);
        if(idObj in ids) {
            scrollTo(main , ids[idObj].position);
        }
    });
});

var current = 1;
setInterval(function() {
    current++;
    current %= (n+1);
    scrollTo(main , current);
} , 5000)


function scrollTo(slider, slide) {
    $(".info").animate({ marginLeft : "-" + (w * (slide - 1)) + "px"}, 1000)
}


