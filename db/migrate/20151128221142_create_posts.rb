class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :topic
      t.string :text
      
      t.attachment :image
      
      t.timestamps null: false
    end
  end
end
